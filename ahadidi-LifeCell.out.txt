*** Life<Cell> 3x11 ***

Generation = 0, Population = 7.
---0----0--
-----0--00-
0-----0----

Generation = 8, Population = 11.
1--*-.-***.
0-----.--.*
0-.-*--*-1.

Generation = 16, Population = 4.
.*.........
*..-.......
**.........

*** Life<Cell> 1x15 ***

Generation = 0, Population = 4.
-0-----0----00-

Generation = 5, Population = 7.
0-----00*.0.1.*

Generation = 10, Population = 2.
--0-1-....-.-..

Generation = 15, Population = 2.
---0-1....-.-..

*** Life<Cell> 15x9 ***

Generation = 0, Population = 13.
---------
-------00
-----0-0-
--0------
---------
---0-----
---------
-----0---
-----0---
---------
--0------
00-------
--0------
-----0---
---------

Generation = 2, Population = 42.
-----0-1-
--0--11-*
---0-0-*-
0--10--10
-----0-0-
-01------
-----1---
-------0-
--00---0-
00---1---
1-0-00---
*-10-0---
1-0-0----
00-0---0-
--0------

Generation = 4, Population = 27.
-----*-10
----1..-.
-0-*--1.-
0----11.-
-----0---
--.-----0
--1--1-1-
0-1------
-0*------
---1-.---
------1--
*-.-0----
-----00--
-*---0---
---------

Generation = 6, Population = 62.
-0---.-**
0-0-1*.-*
-0-.0-**-
0-0*--1.1
-0---*01-
-1*0-0--0
11.11--1-
---10---0
0*.-0101-
-*--1.---
-01-**1-0
.*.*-10--
---00-*10
1.--1--0-
-----10--

Generation = 8, Population = 56.
---0-*1.*
0-0-1..-.
--1.***.1
0-*.1--.-
0--0-.-*1
-*.----10
.-**1-1--
--*1*----
***1*1-.-
-.0*-.0-1
**.-..100
...*----0
*1*0--..*
-.00*----
-0---*---

Generation = 10, Population = 55.
-00--*...
0-*-...-.
--1.*...-
*0.*.-**-
*-0--.-*1
-..0-1010
.1..1-11-
-0.-.--1-
...-.*0.1
-.-.-.*-*
...0.*--*
..**1--00
.-*-0-...
*.0**101-
1001-.-00

Generation = 12, Population = 58.
0*0-1....
0-.-*..-.
00..*.*.-
*-.*.-.*1
.*010.*.-
-..01.---
...*---*0
1-*-**-*-
...-.*0.*
-.0.-**-*
..**.*1-*
.***--**0
.-*---.*.
..*..1---
10*10.10-

*** Life<Cell> 4x10 ***

Generation = 0, Population = 3.
-0---0----
-------0--
----------
----------

Generation = 1, Population = 10.
0-0-0-00--
-0---00-0-
-------0--
----------

Generation = 2, Population = 8.
-0---0-1--
-----1---0
-0---0----
-------0--

Generation = 3, Population = 21.
0-0-01--00
----0-000-
0-0-0100-0
-0---00-0-

Generation = 4, Population = 8.
-----*---1
---0---1-0
-0---*----
-------0--

Generation = 5, Population = 21.
---00.010*
-00-0-0--1
0-000.0--0
-0---00-0-

Generation = 6, Population = 8.
-0-1-.---*
-1--------
-----.-0-1
---0-1----

Generation = 7, Population = 16.
01--0.--0.
0*00---1--
-0-0-.0---
--0---00-0

Generation = 8, Population = 8.
-----.---.
-.--------
-1-10.10-1
-----1-1--

*** Life<Cell> 2x9 ***

Generation = 0, Population = 12.
00---0-00
00-00000-

*** Life<Cell> 2x9 ***

Generation = 0, Population = 4.
-0-00----
--0------

*** Life<Cell> 11x12 ***

Generation = 0, Population = 27.
------------
00-0------00
-0---0-0----
0-----------
0----0-0----
---0------0-
--------0-00
-----00----0
00-----0----
0-----------
0---0--0----

Generation = 10, Population = 60.
.***--1-1.**
*--.1.*.1-1*
*...-**1..-0
*.*---*-*-0.
*****1*.1-1.
*.1.-1...-1-
..**0.*.....
1---.-1**-..
0.**.-0-.-*.
-0...***.---
..1****1-1.-

*** Life<Cell> 15x14 ***

Generation = 0, Population = 4.
--------------
--0-----------
--------------
--------------
------------0-
----------0---
---------0----
--------------
--------------
--------------
--------------
--------------
--------------
--------------
--------------

*** Life<Cell> 8x2 ***

Generation = 0, Population = 11.
0-
0-
00
00
00
0-
--
00

*** Life<Cell> 9x1 ***

Generation = 0, Population = 9.
0
0
0
0
0
0
0
0
0

Generation = 8, Population = 2.
-
0
-
-
-
-
-
0
-

*** Life<Cell> 12x3 ***

Generation = 0, Population = 11.
--0
---
---
-00
000
00-
---
---
0--
--0
---
0--

*** Life<Cell> 12x15 ***

Generation = 0, Population = 4.
----------0----
---------------
----0----------
---------------
---------------
--------0------
---------------
---------------
---------------
-------------0-
---------------
---------------

Generation = 12, Population = 76.
0-0-0-0**---0-0
0--01-.1.0.1000
*--0*....*.*.--
----1-.1.1.0-*-
--*-*--*...-.00
.1..*1*---**-*.
001-.-*---.-*00
-1-1**---1---*-
--00-0--*01---1
00-----...10-01
0---0----1*----
----1*0-.*0-00-

*** Life<Cell> 4x6 ***

Generation = 0, Population = 14.
---000
000-0-
--0-0-
-00000

*** Life<Cell> 14x3 ***

Generation = 0, Population = 7.
-0-
---
00-
--0
-0-
--0
---
---
---
0--
---
---
---
---

Generation = 4, Population = 13.
1--
0-*
.--
1--
**-
*--
10-
--0
-0-
--0
---
---
---
0--

Generation = 8, Population = 17.
.-*
*-*
*--
*0-
.*-
.--
-*-
1--
1*-
---
10-
0-0
-0-
--0

Generation = 12, Population = 20.
.-.
*-*
***
.--
*.1
.*1
1.-
.-*
1*.
-00
---
*-*
*--
1-0

Generation = 16, Population = 18.
.0.
.1*
*.*
*.-
.*.
.*-
-.-
.-.
...
-01
.*1
.-*
*.*
*01

Generation = 20, Population = 14.
*..
*.*
*..
*.-
*.*
.*-
-.0
.-.
...
*-.
...
.0.
.**
.*.

*** Life<Cell> 15x1 ***

Generation = 0, Population = 13.
0
-
0
0
0
0
0
-
0
0
0
0
0
0
0

*** Life<Cell> 2x5 ***

Generation = 0, Population = 10.
00000
00000

Generation = 2, Population = 6.
0-*-0
0-*-0

*** Life<Cell> 4x6 ***

Generation = 0, Population = 1.
---0--
------
------
------

Generation = 17, Population = 9.
0-0-0-
-0-0-0
--0-0-
---0--

*** Life<Cell> 1x3 ***

Generation = 0, Population = 3.
000

Generation = 1, Population = 2.
1-1

Generation = 2, Population = 0.
---

Generation = 3, Population = 0.
---

Generation = 4, Population = 0.
---

Generation = 5, Population = 0.
---

Generation = 6, Population = 0.
---

Generation = 7, Population = 0.
---

Generation = 8, Population = 0.
---

Generation = 9, Population = 0.
---

Generation = 10, Population = 0.
---

Generation = 11, Population = 0.
---

Generation = 12, Population = 0.
---

Generation = 13, Population = 0.
---

Generation = 14, Population = 0.
---

Generation = 15, Population = 0.
---

*** Life<Cell> 2x11 ***

Generation = 0, Population = 11.
00-0---0-0-
00-000-0---

*** Life<Cell> 10x13 ***

Generation = 0, Population = 23.
-------------
00---00----0-
000------0---
------------0
---00-0-0----
0--------0---
-----0-0--0--
-0------0----
-------------
---0-0-------

*** Life<Cell> 12x12 ***

Generation = 0, Population = 2.
------------
---0--------
------------
------------
------------
------------
------------
------------
------------
----------0-
------------
------------

*** Life<Cell> 14x4 ***

Generation = 0, Population = 22.
0000
-000
0---
-00-
----
-0-0
----
----
-00-
-00-
----
-0--
00-0
--00

*** Life<Cell> 6x1 ***

Generation = 0, Population = 5.
0
0
0
0
0
-

*** Life<Cell> 12x12 ***

Generation = 0, Population = 7.
------------
--0---------
-----------0
----0-------
------------
------------
--------0---
-0-------0--
------------
------------
-----------0
------------

*** Life<Cell> 10x8 ***

Generation = 0, Population = 24.
00----0-
--------
----0000
--0-----
--0-----
--0-----
0--0--00
00---0-0
--0---0-
0-0-0--0

Generation = 13, Population = 29.
.*.-.*..
**.1.0**
1*....*1
.-.-*.-*
.-.*1..0
-..*-*-.
.**..-.*
*...*...
*..**...
*....--*

*** Life<Cell> 5x6 ***

Generation = 0, Population = 9.
0-0--0
--0---
-----0
---0--
0---00

Generation = 2, Population = 17.
0--110
1--10-
--1110
-01--0
---0**
